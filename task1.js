//install npm globally(npm install -g) before importing these modules
// importing required packages
const jsonfile = require("jsonfile");
const randomstring = require("randomstring");

//constants for input and output files
const inputFile = "input2.json";
const outputFile = "./output2.json";

// converting an array to a JSON file.
//writeFile() method replaces the specified file and content if it exists
const createJson = (obj) => {
  jsonfile
    .writeFile(outputFile, obj)
    .then(() => {
      console.log(`Write complete at: ${outputFile}`);
    })
    .catch((err) => console.error(err));
};
//creating a function to put random chars in the input
const addRandomChars = (input, amount) => {
  return input + randomstring.generate(amount);
};
//creating a function to get and reverse the name, put 5 randomchar and join with @gmail.com
const createRandomEmails = ({ names }) => {
  const emails = {
    emails: names.map(
      (name) =>
        `${addRandomChars(name.split("").reverse().join(""), 5)}@gmail.com`
    ),
  };
  createJson(emails);
};
//read the inputfile and create an object from it
jsonfile.readFile(inputFile).then((obj) => createRandomEmails(obj));
